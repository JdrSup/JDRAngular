import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { HeroesModule } from './heroes/heroes.module';
import { JdrModule } from './jdr/jdr.module';
import { CreateJdrModule } from './create-jdr/create-jdr.module';
import { CreateHeroModule } from './create-hero/create-hero.module';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { JdrService } from './services/jdr.service';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CreateHeroModule,
    CreateJdrModule,
    JdrModule,
    HeroesModule,
    HomeModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [JdrService],
  bootstrap: [AppComponent]
})
export class AppModule { }
