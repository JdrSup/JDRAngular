import { CreateHeroRoutingModule } from './create-hero-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateHeroComponent } from './container/create-hero/create-hero.component';

@NgModule({
  imports: [
    CommonModule,
    CreateHeroRoutingModule
  ],
  declarations: [CreateHeroComponent]
})
export class CreateHeroModule { }
