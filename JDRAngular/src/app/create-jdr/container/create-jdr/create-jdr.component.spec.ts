import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateJdrComponent } from './create-jdr.component';

describe('CreateJdrComponent', () => {
  let component: CreateJdrComponent;
  let fixture: ComponentFixture<CreateJdrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateJdrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateJdrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
