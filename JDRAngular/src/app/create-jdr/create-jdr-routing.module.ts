import { CreateJdrComponent } from './container/create-jdr/create-jdr.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'createjdr', component: CreateJdrComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateJdrRoutingModule { }
