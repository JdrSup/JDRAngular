import { CreateJdrRoutingModule } from './create-jdr-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateJdrComponent } from './container/create-jdr/create-jdr.component';

@NgModule({
  imports: [
    CommonModule,
    CreateJdrRoutingModule
  ],
  declarations: [CreateJdrComponent]
})
export class CreateJdrModule { }
