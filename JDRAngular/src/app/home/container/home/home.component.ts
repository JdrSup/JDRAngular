import { Component, OnInit } from '@angular/core';
import { JdrService } from '../../../services/jdr.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  jdrs: any;

  constructor(private service: JdrService) { }

  ngOnInit() {
    this.service.getJdr().subscribe((data: any) => this.jdrs = data);
  }

}
