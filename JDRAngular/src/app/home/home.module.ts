import { HomeRoutingModule } from './home-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './container/home/home.component';
import {DataGridModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';


@NgModule({
  imports: [
    CommonModule,
    DataGridModule,
    HomeRoutingModule,
    ButtonModule
  ],
  declarations: [HomeComponent],
  providers: []
})
export class HomeModule { }
