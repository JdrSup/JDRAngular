import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JdrComponent } from './jdr.component';

describe('JdrComponent', () => {
  let component: JdrComponent;
  let fixture: ComponentFixture<JdrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JdrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JdrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
