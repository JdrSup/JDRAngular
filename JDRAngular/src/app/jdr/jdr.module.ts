import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JdrComponent } from './container/jdr/jdr.component';
import { JdrRoutingModule } from './jdr-routing.module';

@NgModule({
  imports: [
    CommonModule,
    JdrRoutingModule
  ],
  declarations: [JdrComponent]
})
export class JdrModule { }
